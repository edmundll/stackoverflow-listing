export default class WebpackStableChunkIdPlugin {
    constructor(options) {
        this.name = 'WebpackStableChunkIdPlugin';
    }
    apply(compiler) {
        compiler.hooks.compilation.tap(this.name, compilation => {
            compilation.hooks.optimizeChunks.tap(`${this.name}OptimizeChunks`, chunks => {
                let idCounter = 0;
                chunks.forEach(chunk => {
                    chunk.id = idCounter++;
                })
            })
        })
    }
}
