export default class WebpackChunkRenderedHashDecoratePlugin {
    /**
     * @typedef {[string|RegExp, string|Function]} replaceArgs
     * @param {{replaceArgsList?: replaceArgs[]}} options
     */
    constructor(options) {
        options = options || {};
        this.name = 'WebpackChunkHashDecoratePlugin';
        this.replaceArgsList = options.replaceArgsList || [];
        this.debugger = [];
    }
    apply(compiler) {
        compiler.hooks.compilation.tap(this.name, compilation => {
            compilation.hooks.contentHash.tap(`${this.name}ContentHash`, chunk => {
                if (this.replaceArgsList && this.replaceArgsList.length) {
                    let newHash = this.replaceArgsList.reduce((hash, replaceArgs) => String.prototype.replace.call(hash, ...replaceArgs), chunk.renderedHash);
                    if (newHash !== chunk.renderedHash) {
                        this.debugger.push(['replaced', chunk.renderedHash, newHash]);
                    }
                    chunk.renderedHash = newHash;
                }
            })
        })
        // compiler.hooks.done.tapPromise(this.name, async () => {
        //     await fsPromise.writeFileAsync('test.json', JSON.stringify(this.debugger));
        // })
    }
}
