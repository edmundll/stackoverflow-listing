require = require('esm')(module);
const TerserPlugin = require('terser-webpack-plugin');
const LodashWebpackPlugin = require('lodash-webpack-plugin');
const webpack = require('webpack');
const generateScopedName = require('./src/lib/GenerateScopedName.js').default;
const WebpackStableChunkIdPlugin = require('./webpack_loaders/WebpackStableChunkIdPlugin.js').default;
const WebpackChunkRenderedHashDecoratePlugin = require('./webpack_loaders/WebpackChunkRenderedHashDecoratePlugin.js').default;
const isDev = process.env.NODE_ENV === 'development';

/**
 * @typedef { import('@vue/cli-service').ProjectOptions } Options
 * @type { Options }
 */
module.exports = {
    lintOnSave: false,
    css: {
        requireModuleExtension: true,
        loaderOptions: {
            css: {
                localsConvention: 'camelCase',
                modules: {
                    getLocalIdent: (context, localIdentName, localName) => {
                        return generateScopedName(localName, context.resourcePath, [
                            /^c-/, // for custom component
                        ], !isDev);
                    }
                },
            }
        }
    },
    configureWebpack: {
        plugins: [
            new LodashWebpackPlugin({
                shorthands: true,
                cloning: true,
                currying: false,
                caching: true,
                collections: true,
                guards: true,
                metadata: false,
                deburring: false,
                unicode: false,
                chaining: true,
                memoizing: false,
                coercions: false,
                flattening: false,
                paths: false,
                placeholders: false,
            }),
            new webpack.IgnorePlugin({
                resourceRegExp: /^\.\/locale$/,
                contextRegExp: /moment$/
            }),
            new WebpackStableChunkIdPlugin(),
            new WebpackChunkRenderedHashDecoratePlugin({
                replaceArgsList: [
                    ['ad', '_-'] // replace "ad" to "_-" to prevent adblock blocking chunk load
                ]
            }),
        ],
        optimization: {
            minimizer: [
                new TerserPlugin({
                    parallel: isDev,
                    terserOptions: {
                        ecma: undefined,
                        warnings: false,
                        parse: {},
                        compress: {
                            drop_console: !isDev
                        },
                        module: false,
                        output: undefined,
                        toplevel: false,
                        nameCache: undefined,
                        ie8: false,
                        keep_fnames: false,
                        keep_classnames: undefined,
                        safari10: true,
                    },
                }),
            ],
            splitChunks: {
                name: 'build',
                chunks: 'all',
                cacheGroups: {
                    // styles
                    main: {
                        name: 'main',
                        test: /assets[\\/]scss[\\/]main\.s?css/i,
                        chunks: 'all',
                        enforce: true,
                        priority: 70,
                    },
                    styles: {
                        name: 'styles',
                        // test: /\.(css|vue)$/,
                        test: module => !/[\\/]node_modules[\\/]vuetify[\\/]/.test(module.context) && module.nameForCondition && /\.(s?css|vue)$/.test(module.nameForCondition()) && !/^javascript/.test(module.type),
                        chunks: 'all',
                        enforce: true,
                        priority: 50,
                    },
                    // javascript
                    lodash: {
                        name: 'lodash',
                        test: /[\\/]node_modules[\\/]lodash/,
                        chunks: 'all',
                        enforce: true,
                        priority: 20,
                    },
                    moment: {
                        name: 'moment',
                        test: /[\\/]node_modules[\\/]moment[\\/]/,
                        chunks: 'all',
                        enforce: true,
                        priority: 20,
                    },
                    vendor: {
                        name: 'vendor',
                        test: /[\\/]node_modules[\\/].*\.(js|vue)$/,
                        chunks: 'all',
                        enforce: true,
                        priority: 15,
                    },
                },
            }
        }
    },
};
