# stackoverflow-listing

## Deploy Project
You can deploy the project by [run on local](#run-on-local) or [run in docker](#run-in-docker)

### Run on Local

#### Project Prerequisite 
- node:13.8 installed
- git installed
- yarn installed

#### Project setup
```
yarn install
```

#### How to run

1. Install the required modules by 
   ```
   yarn install
   ```

2. Build the project 
   ```
   yarn build
   ```

3. Start a local http server with port `8080`
   ```
   yarn local-deploy
   ```

4. Start a browser application and go to `http://localhost:8080`

### Run in Docker

#### Prerequisite
- docker installed
  
#### Setup
```
docker built -t stackoverflow-listing:latest .
```

### Run
1. Start the docker container
   ```
   docker run -p 8080:8080 -it --rm stackoverflow-listing:latest
   ```

2. Start a browser application and go to `http://localhost:8080`
