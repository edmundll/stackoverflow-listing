import VueObserveVisibility from 'vue-observe-visibility';
import Vue, { VueConstructor, PluginObject } from 'vue';

const plugin: PluginObject<any> = {

    install(vue: VueConstructor<Vue>) {
        vue.use(VueObserveVisibility);
    }
};

export default plugin;
