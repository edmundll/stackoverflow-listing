import linkifyStr from 'linkifyjs/string';
import _ from 'lodash';
import Vue, { VueConstructor, PluginObject } from 'vue';

const defaultOptions = {
    defaultProtocol: 'https',
    nl2br: true,
    target: {
        url: '_blank'
    },
}

const plugin: PluginObject<any> = {

    install(vue: VueConstructor<Vue>) {
        vue.filter('linkify', (val: any, options?: ExtendableObject): string => {
            if (!val) return '';
            return linkifyStr(val.toString(), Object.assign(defaultOptions, options || {}));
        });
    }
};

export default plugin;
