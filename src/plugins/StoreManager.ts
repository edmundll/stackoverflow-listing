import Vue, { VueConstructor, PluginObject } from 'vue';
import { getModule } from 'vuex-module-decorators';
import store from '@/store';
import { RootModule, NotifyModule, StackOverflowListingModule } from '@/stores';

export interface IStoreManager {
    root: RootModule;
    notify: NotifyModule;
    stackOverflowListing: StackOverflowListingModule;
}

export const StoreManager: IStoreManager = {
    root: getModule(RootModule, store),
    notify: getModule(NotifyModule, store),
    stackOverflowListing: getModule(StackOverflowListingModule, store),
};

const plugin: PluginObject<any> = {

    install(vue: VueConstructor<Vue>) {
        vue.prototype.$storeManager = StoreManager;
    }
}

export default plugin;
