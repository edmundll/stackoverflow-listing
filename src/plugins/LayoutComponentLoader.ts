import Flex from '@/functionalComponents/Flex';
import Layout from '@/functionalComponents/Layout';
import Spacer from '@/functionalComponents/Spacer';
import Vue, { VueConstructor, PluginObject } from 'vue';

const plugin: PluginObject<any> = {

    install(vue: VueConstructor<Vue>) {
        vue.component<any>('Layout', Layout);
        vue.component<any>('Flex', Flex);
        vue.component<any>('Spacer', Spacer);
    }
}

export default plugin;
