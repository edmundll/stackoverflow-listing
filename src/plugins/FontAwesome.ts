import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faTimes, faCheck, faChevronCircleDown, faPlusCircle, faMinusCircle, faGripVertical, faClipboardList, faSort  } from '@fortawesome/free-solid-svg-icons';
import Vue, { VueConstructor, PluginObject } from 'vue';

library.add(faTimes, faCheck, faChevronCircleDown, faPlusCircle, faMinusCircle, faGripVertical, faClipboardList, faSort);

const plugin: PluginObject<any> = {

    install(vue: VueConstructor<Vue>) {
        vue.component('fa-icon', FontAwesomeIcon);
    }
};

export default plugin;
