import _ from 'lodash';
import Vue, { VueConstructor, PluginObject } from 'vue';

const plugin: PluginObject<any> = {

    install(vue: VueConstructor<Vue>) {
        vue.filter('nl2br', (val: any): string => {
            if(!val) return '';
            return _.escape(val.toString()).replace(/\r?\n/g, '<br>');
        });
    }
};

export default plugin;
