import Axios, { AxiosInstance, AxiosResponse, AxiosError } from 'axios';
import Vue, { PluginObject, VueConstructor } from 'vue';

const axios: AxiosInstance = Axios.create({
    baseURL: process.env.VUE_APP_API_ROOT || '',
    timeout: 60000,
    withCredentials: false,
});

axios.interceptors.request.use((config) => {

    if (config.baseURL !== process.env.VUE_APP_API_ROOT) {
        // use normal call if not server api call
        return config;
    }

    // let token: string = '';
    // config.headers.Authorization = `Bearer ${token}`;
    return config;
});

axios.interceptors.response.use((response: AxiosResponse) => {

    if (response.config.raw) return response;
    return response.data;

}, (error: AxiosError) => {

    // add refresh token logic here
    let oRequest = error.config,
        r = error.response,
        isTokenFailure = false,
        retry = () => {
            return Promise.resolve(() => {
                // refresh token logic here
            }).then(() => {
                if (oRequest.method && oRequest.method.toLowerCase() === 'get') {
                    return axios({
                        url: oRequest.url,
                        method: oRequest.method,
                        params: oRequest.params
                    });
                } else {
                    return axios({
                        url: oRequest.url,
                        method: oRequest.method,
                        data: oRequest.data,
                    })
                }
            })
        };
    if (isTokenFailure) {
        return retry();
    }
    return Promise.reject(error);

});

const plugin: PluginObject<any> = {

    install(vue: VueConstructor<Vue>) {
        vue.prototype.$axios = axios;
    }
};

export {axios};

export default plugin;
