import Ajv from 'ajv';
import Vue, { PluginObject, VueConstructor } from 'vue';

const ajv: Ajv.Ajv = new Ajv();

const plugin: PluginObject<any> = {

    install(vue: VueConstructor<Vue>) {
        vue.prototype.$ajv = ajv;
    }
};

export {ajv};

export default plugin;
