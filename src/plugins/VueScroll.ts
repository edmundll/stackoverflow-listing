import VueScroll from 'vue-scroll';
import Vue, { VueConstructor, PluginObject } from 'vue';

const plugin: PluginObject<any> = {

    install(vue: VueConstructor<Vue>) {
        vue.use(VueScroll);
    }
};

export default plugin;
