import { Tracking } from '@/lib/Tracking';
import Vue, { PluginObject, VueConstructor } from 'vue';

const plugin: PluginObject<any> = {

    install(vue: VueConstructor<Vue>) {
        vue.prototype.$tracking = Tracking;
    }
};

export default plugin;
