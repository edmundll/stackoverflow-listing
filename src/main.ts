import Index from '@/index.vue';
import PluginLoader from '@/lib/PluginLoader';
import * as plugins from '@/plugins';
import Store from '@/store';
import Vue from 'vue';
import '@/assets/scss/font.scss';

new PluginLoader(Object.values(plugins)).load();

// fake vue-i18n
((fn) => {
    Vue.prototype.$t = fn;
    Vue.prototype.$tc = fn;
})((key: string, ...params: any[]) => {
    return key;
});

Vue.config.productionTip = false;

const app = new Vue({
    store: Store,
    render: (h) => h(Index)
})

app.$mount('#app');
