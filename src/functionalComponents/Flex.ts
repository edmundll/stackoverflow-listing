import { FunctionalComponentOptions, CreateElement, RenderContext } from 'vue';
import { MainStyle } from '@/lib/MainStyleLoader.js';

const flex: FunctionalComponentOptions = {
    name: 'flex',
    functional: true,
    render(h: CreateElement, context: RenderContext) {
        const { props, data, children } = context;
        const { staticClass, attrs } = data;
        let staticClassArr = (staticClass || '').split(' ').map(c => c.trim()).map(c => MainStyle[c] || c);
        if (attrs) {
            // reset attrs to extract utility clases like pa-3
            data.attrs = {};
            const classes = Object.keys(attrs).filter(key => {
                // TODO: Remove once resolved
                // https://github.com/vuejs/vue/issues/7841
                if (key === 'slot') return false

                const value = attrs[key]

                // add back data attributes like data-test="foo" but do not
                // add them as classes
                if (key.startsWith('data-') || ['tabindex'].includes(key)) {
                    data.attrs![key] = value
                    return false
                }

                return value || typeof value === 'string'
            })

            if (classes.length) classes.forEach(c => staticClassArr.push(MainStyle[c] || c));
        }
        data.staticClass = `${MainStyle.flex || 'flex'} ${staticClassArr.join(' ')}`;
        if (props.id) {
            data.domProps = data.domProps || {}
            data.domProps.id = props.id
        }
        return h('div', data, children)
    }
}

export default flex;
