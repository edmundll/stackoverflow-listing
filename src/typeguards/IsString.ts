const isString = (val: any): val is string => {
    return typeof(val) === 'string';
}

export default isString;
