import ValidationRule, { ValidationRuleType, ValidationRuleParam } from '@/types/ValidationRule';

const i18n = {
    t(str: string, ...params: any[]): string {
        return str.replace('validationRules.', '');
    },
    tc(str: string, ...params: any[]): string {
        return str.replace('validationRules.', '');
    }
}

const HKPhoneRegexRaw = /^[23456789][0-9]{7}$/;
const HKPhoneRegex = /^(852)?[23456789][0-9]{7}$/;
const ChinaPhoneRegex = /^(86)?1[0-9]{10}$/;

const getValidationErrorMessage = <T>(type: ValidationRuleType, params: ValidationRuleParam<T>, fallback: string): string => {
    if(params.message) {
        if(typeof(params.message) === 'string') {
            return params.message;
        }else if(params.message[type]) {
            return params.message[type] as string;
        }else if(params.message.default) {
            return params.message.default;
        }
    }
    return fallback;
};

const validationRules: { [key in ValidationRuleType]: ValidationRule} = {
    REQUIRED: <T = string | number | boolean>(v, params) => !!(v) && v.toString().trim().length > 0 ||
        getValidationErrorMessage('REQUIRED', params, i18n.t('validationRules.required', {label: params.label || i18n.t('validationRules.thisField')}) as string),

    REQUIRED_IF: <T = string | number | boolean>(v, params) => !(params.ifFunction && params.ifFunction()) ||
        !!(v) && (!v.toString().trim || v.toString().trim().length > 0) ||
        getValidationErrorMessage('REQUIRED_IF', params, i18n.t('validationRules.required', {label: params.label || i18n.t('validationRules.thisField')}) as string),

    MIN: <T = string>(v, params) => !(v) || v.toString().trim().length >= (params.min || 0) ||
        getValidationErrorMessage('MIN', params, i18n.tc('validationRules.min', params.min, { label: params.label || i18n.t('validationRules.thisField'), min: params.min}) as string),

    MAX: <T = string>(v, params) => !(v) || v.toString()/*.trim()*/.length <= (params.max || 999) ||
        getValidationErrorMessage('MAX', params, i18n.tc('validationRules.max', params.max, { label: params.label || i18n.t('validationRules.thisField'), max: params.max}) as string),

    MINMAX: <T = string>(v, params) => !(v) || (v.toString().trim().length >= (params.min || 0) && v.toString()/*.trim()*/.length <= (params.max || 999)) ||
        getValidationErrorMessage('MINMAX', params, i18n.t('validationRules.minMax', { label: params.label || i18n.t('validationRules.thisField'), min: params.min, max: params.max}) as string),

    SIZE: <T = string>(v, params) => !(v) || v.toString()/*.trim()*/.length === parseInt(params.size) ||
        getValidationErrorMessage('SIZE', params, i18n.tc('validationRules.size', params.size, { label: params.label || i18n.t('validationRules.thisField'), size: params.size}) as string),

    PHONE: <T = string>(v, params) => !v || (params.regex ? [params.regex] : [HKPhoneRegex, ChinaPhoneRegex]).some(regex => regex.test(v)) ||
        getValidationErrorMessage('PHONE', params, i18n.t('validationRules.phone', { label: params.label || i18n.t('validationRules.thisField') }) as string),

    EMAIL: <T = string>(v, params) => !v || (params.regex || /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(v) ||
        getValidationErrorMessage('EMAIL', params, i18n.t('validationRules.email', { label: params.label || i18n.t('validationRules.thisField') }) as string),

    DATE: <T = string>(v, params) => !v || /^[0-9]{4}\-[0-1][0-9]\-[0-3][0-9]$/.test(v) ||
        getValidationErrorMessage('DATE', params, i18n.t('validationRules.date', { label: params.label || i18n.t('validationRules.thisField') }) as string),

    TIME: <T = string>(v, params) => !v || /^[0-2][0-9]:[0-5][0-9]$/.test(v) ||
        getValidationErrorMessage('TIME', params, i18n.t('validationRules.time', { label: params.label || i18n.t('validationRules.thisField') }) as string),

    INTEGER: <T = string>(v, params) => !v || /^[0-9]+$/.test(v) ||
        getValidationErrorMessage('INTEGER', params, i18n.t('validationRules.integer', {label: params.label || i18n.t('validationRules.thisField')}) as string),

    FLOAT: <T = string>(v, params) => !v || /^[0-9]+(\.[0-9]+)?$/.test(v) ||
        getValidationErrorMessage('FLOAT', params, i18n.t('validationRules.float', {label: params.label || i18n.t('validationRules.thisField')}) as string),

    REGEX: <T = string>(v, params) => !v || (params.regex || /^.*$/).test(v) ||
        getValidationErrorMessage('REGEX', params, i18n.t('validationRules.regex', {label: params.label || i18n.t('validationRules.thisField')}) as string),

    IN: <T = string | number | boolean>(v, params) => !v || (params.values || []).includes(v) ||
        getValidationErrorMessage('IN', params, i18n.t('validationRules.in', {label: params.label || i18n.t('validationRules.thisField'), optionString: params.values.join(', ')}) as string),

    ARRAY_REQUIRED: <T = any[]>(v, params) => (!!(v) && v.length > 0) ||
        getValidationErrorMessage('ARRAY_REQUIRED', params, i18n.t('validationRules.arrayRequired', {label: params.label || i18n.t('validationRules.thisField')}) as string),

    ARRAY_MIN: <T = any[]>(v, params) => !v || v.length >= (params.min || 0) ||
        getValidationErrorMessage('ARRAY_MIN', params, i18n.tc('validationRules.arrayMin', params.min, { label: params.label || i18n.t('validationRules.thisField'), min: params.min}) as string),

    ARRAY_MAX: <T = any[]>(v, params) => !v || v.length <= (params.max || 999) ||
        getValidationErrorMessage('ARRAY_MAX', params, i18n.tc('validationRules.arrayMax', params.max, { label: params.label || i18n.t('validationRules.thisField'), max: params.max}) as string),

    ARRAY_MINMAX: <T = any[]>(v, params) => !(v) || (v.length >= (params.min || 0) && v.length <= (params.max || 999)) ||
        getValidationErrorMessage('ARRAY_MINMAX', params, i18n.t('validationRules.arrayMinMax', { label: params.label || i18n.t('validationRules.thisField'), min: params.min, max: params.max}) as string),

    ARRAY_SIZE: <T = any[]>(v, params) => !(v) || v.length === parseInt(params.size) ||
        getValidationErrorMessage('ARRAY_SIZE', params, i18n.tc('validationRules.arraySize', params.size, { label: params.label || i18n.t('validationRules.thisField'), size: params.size}) as string),

    CUSTOM_VALIDATOR: <T = any>(v, params) => !params.validator || params.validator(v) ||
        getValidationErrorMessage('CUSTOM_VALIDATOR', params, i18n.t('validationRules.customValidator', {label: params.label || i18n.t('validationRules.thisField')}) as string),
}

export default validationRules;
