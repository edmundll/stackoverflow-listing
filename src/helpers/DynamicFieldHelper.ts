import _ from 'lodash';
import { DynamicFieldType, FieldOption, ParsedFieldOption } from '@/types/components/DynamicField';
import ValidationRule from '@/types/ValidationRule';
import ValidationRules from '@/constants/ValidationRules';
import Vue from 'vue';

export class ValidationError extends Error {

    public validationErrorMessages: string[] | undefined;

    constructor(...args: any[]) {
        super(...args);
    }
}

export const getValueByPathParts = (obj: any, pathParts: string[]): any => {
    if (typeof (obj) !== 'object') return undefined;
    if (!pathParts.length) return obj;
    if (pathParts.length === 1) {
        return obj[pathParts[0]];
    }
    let nextPathArr = [...pathParts],
        nextKey = nextPathArr.shift();
    return getValueByPathParts(obj[nextKey!], nextPathArr);
};


export const parseObjectPath = (path: string): string[] => {
    const removeQuotes = (s: string) => s.replace(/['"]/g, '');
    let matches = path.match(/[^."'[\]]+|"([^"]+")|'([^']+')/g);
    if(!matches) return [];
    return matches.map(removeQuotes);
};


export const parseTemplate = (templateRootName: string, fields: FieldOption[], vm: Vue, dataRootName: string, customParseFn?: (field: FieldOption) => void, parent?: string): ParsedFieldOption[] => {
    let parsed: ParsedFieldOption[] = [];
    fields.forEach(f => {
        let c: FieldOption = _.cloneDeep(f);
        c.templateRootName = templateRootName;
        c._name = f.name;
        if (parent) {
            c.name = `${parent}[${f.name}]`;
        }
        if (c.type === DynamicFieldType.CUSTOM) {
            c.children = parseTemplate(templateRootName, c.children, vm, dataRootName, customParseFn, c.name);
        }
        if (customParseFn) {
            customParseFn(c);
        }
        if (c.requiredIf) {
            let originalRequiredIfFn: (() => boolean) | undefined = c.requiredIfFn;
            c.requiredIfFn = () => {
                let originalRequiredIfFnResult = originalRequiredIfFn ? originalRequiredIfFn() : false;
                return originalRequiredIfFnResult || (() => {
                    let split = c.requiredIf!.split(','),
                        pathParts = parseObjectPath(split[0]),
                        target = getValueByPathParts(vm[dataRootName], pathParts);
                    if (split.length < 2) {
                        return !!target;
                    } else {
                        switch (split[1]) {
                            case 'true':
                                return !!target;
                            case 'false':
                                return !target;
                            default:
                                return target == split[1];
                        }
                    }
                })();
            }
        }
        // get rules
        c.rules = getRules(c);

        if (c.type === DynamicFieldType.ARRAY) {
            let typeOfParsed = parseTemplate(templateRootName, [c.typeOf], vm, dataRootName, customParseFn)
            c.typeOf = typeOfParsed[0] || c.typeOf;
        }

        parsed.push(c as ParsedFieldOption);

        // if (f.type === DynamicFieldType.CUSTOM) {
        //     f.children.forEach(c => {
        //         parsed.push(c);
        //     })
        // }

    });
    return parsed;
};

export const getRules = (field: FieldOption): ValidationRule[] => {
    let rules: ValidationRule[] = [];
    if (field.hidden) {
        return rules;
    }
    if ([
        DynamicFieldType.STRING,
        DynamicFieldType.INTEGER,
        DynamicFieldType.FLOAT,
        DynamicFieldType.DATE,
        DynamicFieldType.PHONE,
        DynamicFieldType.EMAIL,
        DynamicFieldType.PASSWORD].includes(field.type) ||
        (field.type === DynamicFieldType.LIST && !field.multiple)) {
        if (field.required) {
            rules.push(v => ValidationRules.REQUIRED.call(null, v, { label: field.label, message: field.errorMessage }));
        }
        if (field.requiredIfFn) {
            rules.push(v => ValidationRules.REQUIRED_IF.call(null, v, {
                label: field.label,
                ifFunction: field.requiredIfFn,
                message: field.errorMessage,
            }));
        }
        if ((field as any).regex) {
            rules.push(v => ValidationRules.REGEX.call(null, v, {
                label: field.label,
                regex: (field as any).regex,
                message: field.errorMessage,
            }));
        }
        if ((field as any).size !== undefined) {
            rules.push(v => ValidationRules.SIZE.call(null, v, {
                label: field.label,
                size: (field as any).size,
                message: field.errorMessage,
            }));
        } else if ((field as any).min !== undefined && (field as any).max) {
            rules.push(v => ValidationRules.MINMAX.call(null, v, {
                label: field.label,
                min: (field as any).min,
                max: (field as any).max,
                message: field.errorMessage,
            }));
        } else if ((field as any).min !== undefined) {
            rules.push(v => ValidationRules.MIN.call(null, v, {
                label: field.label,
                min: (field as any).min
            }));
        } else if ((field as any).max !== undefined) {
            rules.push(v => ValidationRules.MAX.call(null, v, {
                label: field.label,
                max: (field as any).max,
                message: field.errorMessage,
            }));
        }
        if (field.type === DynamicFieldType.PHONE) {
            rules.push(v => ValidationRules.PHONE.call(null, v, {
                label: field.label,
                message: field.errorMessage,
                regex: (field as any).regex,
            }));
        }
        if (field.type === DynamicFieldType.EMAIL) {
            rules.push(v => ValidationRules.EMAIL.call(null, v, {
                label: field.label,
                message: field.errorMessage,
            }));
        }
        if (field.type === DynamicFieldType.DATE) {
            rules.push(v => ValidationRules.DATE.call(null, v, {
                label: field.label,
                message: field.errorMessage,
            }));
        }
        if (field.type === DynamicFieldType.INTEGER) {
            rules.push(v => ValidationRules.INTEGER.call(null, v, {
                label: field.label,
                message: field.errorMessage,
            }))
        }
        if (field.type === DynamicFieldType.FLOAT) {
            rules.push(v => ValidationRules.FLOAT.call(null, v, {
                label: field.label,
                message: field.errorMessage,
            }));
        }
    } else if ([
        DynamicFieldType.ARRAY,
        DynamicFieldType.LIST].includes(field.type)) {
        if (field.required) {
            rules.push(v => ValidationRules.ARRAY_REQUIRED.call(null, v, {
                label: field.label,
                message: field.errorMessage,
            }));
        }
        if ((field as any).size !== undefined) {
            rules.push(v => ValidationRules.ARRAY_SIZE.call(null, v, {
                label: field.label,
                size: (field as any).size,
                message: field.errorMessage,
            }));
        } else if ((field as any).min !== undefined && (field as any).max !== undefined) {
            rules.push(v => ValidationRules.ARRAY_MINMAX.call(null, v, {
                label: field.label,
                min: (field as any).min,
                max: (field as any).max,
                message: field.errorMessage,
            }));
        } else if ((field as any).min !== undefined) {
            rules.push(v => ValidationRules.ARRAY_MIN.call(null, v, {
                label: field.label,
                min: (field as any).min,
                message: field.errorMessage,
            }));
        } else if ((field as any).max !== undefined) {
            rules.push(v => ValidationRules.ARRAY_MAX.call(null, v, {
                label: field.label,
                max: (field as any).max,
                message: field.errorMessage,
            }));
        }
    }
    if (field.validator) {
        rules.push(v => ValidationRules.CUSTOM_VALIDATOR.call(null, v, {
            label: field.label,
            message: field.errorMessage,
            validator: field.validator,
        }));
    }
    return rules;
};

export const getAllNestedFields = (fields: Array<ParsedFieldOption | FieldOption>): Array<ParsedFieldOption | FieldOption> => {
    let allFields: Array<ParsedFieldOption | FieldOption> = [];
    fields.forEach(f => {
        allFields.push(f);
        if(f.type === DynamicFieldType.CUSTOM) {
            allFields = _.concat(allFields, getAllNestedFields(f.children));
        }
    });
    return allFields;
}

export const initFields = (field: ParsedFieldOption, vm: Vue, dataRootName: string, customInitFn?: (field: ParsedFieldOption, vm: Vue, dataRootName: string) => void, parentPath?: string): void => {
    // get parent obj if have parent path
    let parentObj = parentPath ? getValueByPathParts(vm[dataRootName], parseObjectPath(parentPath)) : undefined;
    if (!parentObj && field._name !== field.name) return;
    // init by type
    switch (field.type) {
        case DynamicFieldType.INTEGER:
        case DynamicFieldType.FLOAT:
        case DynamicFieldType.STRING:
        case DynamicFieldType.DATE:
        case DynamicFieldType.PHONE:
        case DynamicFieldType.EMAIL:
        case DynamicFieldType.PASSWORD:
            if (parentObj && field._name)
                Vue.set(parentObj, field._name, '');
            else
                Vue.set(vm[dataRootName], field.name, '');
            break;
        case DynamicFieldType.ARRAY:
            if (parentObj && field._name)
                Vue.set(parentObj, field._name, []);
            else
                Vue.set(vm[dataRootName], field.name, []);
            break;
        case DynamicFieldType.LIST:
            if (field.multiple) {
                if (parentObj && field._name)
                    Vue.set(parentObj, field._name, []);
                else
                    Vue.set(vm[dataRootName], field.name, []);
            } else {
                if (parentObj && field._name)
                    Vue.set(parentObj, field._name, '');
                else
                    Vue.set(vm[dataRootName], field.name, '');
            }
            break;
        case DynamicFieldType.CUSTOM:
            if (parentObj && field._name)
                Vue.set(parentObj, field._name, {});
            else
                Vue.set(vm[dataRootName], field.name, {});
            break;
        case DynamicFieldType.SEPARATOR:
            if (parentObj && field._name)
                Vue.set(parentObj, field._name, field.description);
            else
                Vue.set(vm[dataRootName], field.name, field.description);
            break;
    }
    // init by name
    if (customInitFn) {
        customInitFn(field, vm, dataRootName);
    }
    // init child
    if (field.type === DynamicFieldType.CUSTOM) {
        field.children.forEach(c => initFields(c, vm, dataRootName, customInitFn, `${parentPath ? parentPath + '.' : ''}${field._name}`));
    }
};

export const getFormDataFromObj = (valueObj: ExtendableObject, fieldName2field: Dictionary<ParsedFieldOption>, validateTemplateRoots: string[]) => {
    let form = new FormData(),
        appendToForm = (fieldName: string, input: any): void => {
            let field = fieldName2field[fieldName] || {};
            input = getProcessedValueForFormData(field, input, validateTemplateRoots.includes(field.templateRootName || ''));
            if ((input instanceof Object && !(input instanceof Blob)) || input instanceof Array) {
                if (_.size(input) === 0) {
                    appendToForm(`${fieldName}[]`, '');
                } else {
                    _.forEach(input, (v, k) => {
                        appendToForm(`${fieldName}[${k}]`, v);
                    })
                }
            } else {
                form.append(fieldName, input);
            }
        };
    _.forEach(valueObj, (value, fieldName) => {
        appendToForm(fieldName, value);
    });
    return form;
};

export const getProcessedValueForFormData = (field: ParsedFieldOption, value: any, validate: boolean): any => {
    let _value = value;
    // determine by type
    switch (field.type) {
        case DynamicFieldType.LIST:
        case DynamicFieldType.STRING:
        case DynamicFieldType.INTEGER:
        case DynamicFieldType.DATE:
        case DynamicFieldType.PHONE:
        case DynamicFieldType.EMAIL:
        case DynamicFieldType.FLOAT:
        case DynamicFieldType.PASSWORD:
            _value = value || '';
            break;
        case DynamicFieldType.ARRAY:
            _value = value ? value.map(v => getProcessedValueForFormData(field.typeOf, v, validate)) : [];
            break;
        case DynamicFieldType.CUSTOM:
            _value = Object.assign({}, value);
            field.children.forEach(childField => {
                _value[childField._name] = getProcessedValueForFormData(childField, _value[childField._name], validate);
            })
            break;
        default:
            break;
    }
    // validate value
    if (validate && field.rules && field.rules.length) {
        let errorMessages = field.rules.reduce((messages: string[], r: ValidationRule) => {
            let result = r(_value);
            if (typeof (result) === 'string') messages.push(result);
            return messages;
        }, []);
        if (errorMessages.length) {
            let error = new ValidationError('ValidationError');
            error.validationErrorMessages = errorMessages;
            throw error;
        }
    }
    return _value;
};
