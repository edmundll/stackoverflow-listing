const VueRouter = {
    currentRoute: {
        fullPath: '',
    }
}

const VueI18n = {
    t(str: string, ...params: any[]): string {
        return str;
    },
}
import _ from 'lodash';
import { WebPageAcceptableField, getWebPageJsonLdObject, WebSiteAcceptableField, getWebSiteJsonLdObject, getImageObjectJsonLdObject, PersonAcceptableField, getPersonJsonLdObject, getJsonLdFromObject, getBreadcrumbListJsonLdObject, getListItemsFromArray, getUrlByRoute } from '@/lib/JsonLdBuilder';
import {
    WebSite,
    WebPage,
    BlogPosting,
    LocalBusiness,
    ImageObject,
    Organization,
    Person,
    SearchAction,
    Thing,
    Place,
    GeoCoordinates,
    AggregateRating,
    BreadcrumbList,
    EntryPoint,
    ListItem,
    ItemList,
    OpeningHoursSpecification,
    DayOfWeekEnum,
} from 'schema-dts';

export const getBreadcrumbList = (arr: Thing[]): BreadcrumbList | undefined => {
    if (arr.length === 0) return undefined;
    return getBreadcrumbListJsonLdObject({
        itemListElement: getListItemsFromArray([
            getWebSiteJsonLdObject({
                '@id': process.env.VUE_APP_BASE_URL,
                name: `${VueI18n.t('home') as string} | ${VueI18n.t('meta.titles.base') as string}`,
            }),
            ...arr
        ]),
        numberOfItems: arr.length + 1,
    })
};

export const getBasicWebPageObject = (override?: { [key in WebPageAcceptableField]?: WebPage[key] }): WebPage => {
    let currentRoute = VueRouter.currentRoute;
    return getWebPageJsonLdObject({
        name: `${VueI18n.t('home') as string} | ${VueI18n.t('meta.titles.base') as string}`,
        url: process.env.VUE_APP_BASE_URL + currentRoute.fullPath,
        description: VueI18n.t('meta.descriptions.default'),
        isPartOf: getBasicWebSiteObject(),
        ...(override && override.mainEntity ? _.pickBy(override.mainEntity, (v, k) => ['name', 'url', 'description', 'keywords'].indexOf(k) >= 0) : {}),
        ...(override && override.mainEntity && (override.mainEntity as any).image ? {
            primaryImageOfPage: getImageObjectJsonLdObject({
                url: (override.mainEntity as any).image
            })
        } : {}),
        ...(override || {}),
    })
};

export const getBasicWebSiteObject = (override?: { [key in WebSiteAcceptableField]?: WebSite[key] }): WebSite => {
    return getWebSiteJsonLdObject({
        name: `${VueI18n.t('home') as string} | ${VueI18n.t('meta.titles.base') as string}`,
        url: process.env.VUE_APP_BASE_URL,
        description: VueI18n.t('meta.descriptions.default') as string,
    })
};

export const getDefaultSiteJsonLd = (): string => {
    return getJsonLdFromObject<WebSite>(getBasicWebSiteObject(), true);
};

export const getDefaultPageJsonLd = (): string => {
    return getJsonLdFromObject<WebPage>(getBasicWebPageObject(), true);
};
