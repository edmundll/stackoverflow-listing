import {
    WebSite,
    WebPage,
    BlogPosting,
    LocalBusiness,
    ImageObject,
    Organization,
    Person,
    SearchAction,
    Thing,
    Place,
    GeoCoordinates,
    AggregateRating,
    BreadcrumbList,
    EntryPoint,
    ListItem,
    ItemList,
    OpeningHoursSpecification,
    DayOfWeekEnum,
} from 'schema-dts';
import _ from 'lodash';
import moment from 'moment';

export type AcceptableType = WebSite | WebPage;

export type ThingAcceptableField = 'description' | 'image' | 'mainEntityOfPage' | 'name' | 'potentialAction' | 'url';
export type GeoCoordinatesAcceptableField = 'address' | 'latitude' | 'longitude' | ThingAcceptableField;
export type RatingAcceptableField = 'bestRating' | 'worstRating' | 'ratingValue' | ThingAcceptableField;
export type AggregateRatingAcceptableField = 'ratingCount' | RatingAcceptableField | ThingAcceptableField;
export type PersonAcceptableField = 'address' | 'birthDate' | 'deathDate' | 'email' | 'gender' | 'telephone' | ThingAcceptableField;
export type MediaObjectAcceptableField = 'width' | 'height' | CreativeWorkAcceptableField | ThingAcceptableField;
export type ImageObjectAcceptableField = 'caption' | 'representativeOfPage' | 'thumbnail' | MediaObjectAcceptableField | ThingAcceptableField;
export type EntryPointAcceptableField = 'urlTemplate' | 'httpMethod' | ThingAcceptableField;
export type ListItemAcceptableField = 'item' | 'position' | ThingAcceptableField;
export type ItemListAcceptableField = 'itemListElement' | 'numberOfItems' | ThingAcceptableField;
export type BreadcrumbListAcceptableField = ItemListAcceptableField | ThingAcceptableField;
export type ActionAcceptableField = 'target' | ThingAcceptableField;
export type SearchActionAcceptableField = ActionAcceptableField | ThingAcceptableField;
export type ArticleAcceptableField = 'articleBody' | CreativeWorkAcceptableField | ThingAcceptableField;
export type OpeningHoursSpecificationAcceptableField = 'dayOfWeek' | 'opens' | 'closes' | ThingAcceptableField;

export type OrganizationAcceptableField = 'address' | 'aggregateRating' | 'email' | 'founder' | 'foundingDate' | 'member' | 'knowsLanguage' | 'location' | 'logo' | 'telephone' | 'parentOrganization' | ThingAcceptableField;
export type PlaceAcceptableField = 'address' | 'aggregateRating' | 'geo' | 'hasMap' | 'logo' | 'maximumAttendeeCapacity' | 'openingHoursSpecification' | 'photo' | 'telephone' | ThingAcceptableField;
export type LocalBusinessAcceptableField = 'openingHours' | 'priceRange' | OrganizationAcceptableField | PlaceAcceptableField | ThingAcceptableField;
export type CreativeWorkAcceptableField = 'about' | 'author' | 'copyrightHolder' | 'creator' | 'dateCreated' | 'dateModified' | 'datePublished' | 'inLanguage' | 'isPartOf' | 'headline' | 'keywords' | 'mainEntity' | 'provider' | 'publisher' | 'thumbnailUrl' | ThingAcceptableField;
export type BlogPostingAcceptableField = ArticleAcceptableField | ThingAcceptableField;

export type WebPageAcceptableField = 'breadcrumb' | 'primaryImageOfPage' | CreativeWorkAcceptableField | ThingAcceptableField;
export type WebSiteAcceptableField = CreativeWorkAcceptableField | ThingAcceptableField;

export const getUrlByRoute = () => '';

export const getThingJsonLdObject = (input: {[key in Extract<Thing, ThingAcceptableField>]?: Thing[key]}): Thing => {
    return {
        '@type': 'Thing',
        ...input
    }
};

export const getGeoCoordinatesJsonLdObject = (input: {[key in GeoCoordinatesAcceptableField]?: GeoCoordinates[key]}): GeoCoordinates => {
    return {
        '@type': 'GeoCoordinates',
        ...input,
    }
};

export const getAggregateRatingJsonLdObject = (input: {[key in AggregateRatingAcceptableField]?: AggregateRating[key]}): AggregateRating => {
    return {
        '@type': 'AggregateRating',
        ...input,
    }
};

export const getPersonJsonLdObject = (input: { [key in PersonAcceptableField]?: Person[key] }): Person => {
    return {
        '@type': 'Person',
        ...input,
    }
};

export const getOrganizationJsonLdObject = (input: {[key in OrganizationAcceptableField]?: Organization[key]}): Organization => {
    return {
        '@type': 'Organization',
        ...input
    }
};

export const getPlaceJsonLdObject = (input: {[key in PlaceAcceptableField]?: Place[key]}): Place => {
    return {
        '@type': 'Place',
        ...input
    }
};

export const getImageObjectJsonLdObject = (input: {[key in ImageObjectAcceptableField]?: ImageObject[key]}): ImageObject => {
    return {
        '@type': 'ImageObject',
        ...input
    }
};

export const getEntryPointJsonLdObject = (input: {[key in EntryPointAcceptableField]?: EntryPoint[key]}): EntryPoint => {
    return {
        '@type': 'EntryPoint',
        ...input
    }
};

export const getListItemJsonLdObject = (input: {[key in ListItemAcceptableField]?: ListItem[key]}): ListItem => {
    return {
        '@type': 'ListItem',
        ...input
    }
};

export const getItemListJsonLdObject = (input: {[key in ItemListAcceptableField]?: ItemList[key]}): ItemList => {
    return {
        '@type': 'ItemList',
        ...input
    }
};

export const getBreadcrumbListJsonLdObject = (input:{[key in BreadcrumbListAcceptableField]?: BreadcrumbList[key]}): BreadcrumbList => {
    return {
        '@type': 'BreadcrumbList',
        ...input,
    }
};

export const getSearchActionJsonLdObject = (input: {[key in SearchActionAcceptableField]?: SearchAction[key]} & {'query-input'?: string}): SearchAction => {
    return {
        '@type': 'SearchAction',
        ...input
    } as SearchAction;
};

export const getOpeningHoursSpecificationJsonLdObject = (input:{[key in OpeningHoursSpecificationAcceptableField]?: OpeningHoursSpecification[key]}): OpeningHoursSpecification => {
    return {
        '@type': 'OpeningHoursSpecification',
        ...input,
    }
};

export const getLocalBusinessJsonLdObject = (input: {[key in LocalBusinessAcceptableField]?: LocalBusiness[key]} & {'@id'?: string}): LocalBusiness => {
    return {
        '@type': 'LocalBusiness',
        ...input
    } as LocalBusiness
};

export const getBlogPostingJsonLdObject = (input: {[key in BlogPostingAcceptableField]?: BlogPosting[key]}): BlogPosting => {
    return {
        '@type': 'BlogPosting',
        ...input
    }
};

export const getWebPageJsonLdObject = (input: {[key in WebPageAcceptableField]?: WebPage[key]} & {'@id'?: string}): WebPage => {
    return {
        '@type': 'WebPage',
        ...input
    } as WebPage
};

export const getWebSiteJsonLdObject = (input: {[key in WebSiteAcceptableField]?: WebSite[key]} & {'@id'?: string}): WebSite => {
    return {
        '@type': 'WebSite',
        ...input
    } as WebSite
};

export const getOpeningHoursSpecificationFromOpeningHours = (openingHours: string[]): OpeningHoursSpecification[] => {
    moment.locale('en');
    return openingHours.map(string => {
        let [days, times] = string.split(' ');
        if(days && times) {
            let dayIndex2dayOfWeekEnum: {[key: number]: DayOfWeekEnum} = {
                    0: DayOfWeekEnum.Sunday,
                    1: DayOfWeekEnum.Monday,
                    2: DayOfWeekEnum.Tuesday,
                    3: DayOfWeekEnum.Wednesday,
                    4: DayOfWeekEnum.Thursday,
                    5: DayOfWeekEnum.Friday,
                    6: DayOfWeekEnum.Saturday,
                },
                dayOfWeek: DayOfWeekEnum[] = days.split(',').map(d => dayIndex2dayOfWeekEnum[moment().day(d).day()]),
                [opens, closes] = times.split('-');
            return getOpeningHoursSpecificationJsonLdObject({
                opens,
                closes,
                dayOfWeek,
            })
        }
        return null;
    }).filter(spec => !!spec) as OpeningHoursSpecification[];
};

export const getListItemsFromArray = (arr: Thing[]): ListItem[] => {
    return arr.map((thing, i) => getListItemJsonLdObject({item: thing, position: i + 1}));
};

export const getJsonLdFromObject = <T extends AcceptableType>(input: T, isRoot: boolean = false): string => {
    if(isRoot) {
        return JSON.stringify(Object.assign({'@context': 'http://schema.org'}, input));
    }
    return JSON.stringify(input);
};
