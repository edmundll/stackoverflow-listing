import incstr from "incstr";
import crypto from "crypto";

const index = {};
const reverse = {};
const resultIndex = {};

const generateNextId = incstr.idGenerator({
  // Removed "d" letter to avoid accidental "ad" construct.
  // @see https://medium.com/@mbrevda/just-make-sure-ad-isnt-being-used-as-a-class-name-prefix-or-you-might-suffer-the-wrath-of-the-558d65502793
  alphabet: "abcefghijklmnopqrstuvwxyz0123456789"
});

const uniqueIdGenerator = name => {
  if (index[name]) {
    return index[name];
  }

  let nextId,
    safeCount = 10;

  do {
    // Class name cannot start with a number.
    nextId = generateNextId();
  } while (/^[0-9]/.test(nextId));

  while (reverse[nextId] && safeCount > 0) {
    // nextId should not collide
    nextId = generateNextId();
    safeCount--;
  }

  index[name] = nextId;
  reverse[nextId] = name;

  return nextId;
};

const generateScopedName = (localName, resourcePath, exceptions, doHash) => {
  if (exceptions.some(v => v.test(localName))) {
    return localName;
  }
  let pathParts = resourcePath.split("/"),
    componentName = pathParts.pop(),
    namePrefix = `_${
      doHash
        ? crypto
            .createHash("md5")
            .update(pathParts.join("/"))
            .digest("hex")
        : pathParts.join("_")
    }`,
    hash = `_${
      doHash
        ? crypto
            .createHash("md5")
            .update(localName)
            .digest("hex")
        : localName
    }`,
    prefix = `_${
      doHash
        ? crypto
            .createHash("md5")
            .update(`${namePrefix}-${componentName}`)
            .digest("hex")
        : `${uniqueIdGenerator(`${namePrefix}|${componentName}`)}`
    }`,
    result = (doHash
      ? `${prefix.substr(0, 5)}${hash.substr(0, 6)}`
      : `${prefix}-${hash}`
    ).replace(/\./g, "_");
  while (
    resultIndex[result] &&
    `${resourcePath}|${localName}` !== resultIndex[result]
  ) {
    result = `${result}--1`;
  }
  resultIndex[result] = `${resourcePath}|${localName}`;
  return result;
};

export default generateScopedName;
