export const getPath = (e: Event): Array<EventTarget | Element> => {
    if ('path' in e) return (e as any).path as EventTarget[];
    //polyfill
    const path: Array<EventTarget | Element> = [];
    let currentElem = e.target;
    while (currentElem) {
        path.push(currentElem);
        currentElem = (currentElem as Element).parentElement;
    }
    if (path.indexOf(window) === -1 && path.indexOf(document) === -1) path.push(document);
    if (path.indexOf(window) === -1) path.push(window);
    return path;
}
