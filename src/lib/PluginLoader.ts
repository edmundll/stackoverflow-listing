import Vue, { PluginObject } from 'vue';

export default class PluginLoader {

    public plugins: Array<PluginObject<any>> = [];

    constructor(plugins: Array<PluginObject<any>>) {
        this.plugins = plugins;
    }

    public load() {
        this.plugins.forEach((plugin: PluginObject<any>) => {
            Vue.use(plugin);
        })
    }
}
