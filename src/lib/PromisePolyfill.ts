export const PromiseEach = async <T = any>(arr: T[], fn: (item: T, index: number, arr: T[]) => Promise<void | boolean>): Promise<void> => {
    return arr.reduce((prev: Promise<void | boolean>, cur: T, i: number) => {
        return prev.then((prevResult: void | boolean) => {
            if(prevResult === true) {
                // stop executing
                return Promise.resolve();
            }
            return Promise.resolve(fn(cur, i, arr));
        })
    }, Promise.resolve()).then(() => void(0));
};

export const PromiseMap = async <T = any, R = any>(arr: T[], fn: (item: T, index: number, arr: T[]) => Promise<R>): Promise<R[]> => {
    let newArr: R[] = [];
    return PromiseEach(arr, async (item, i, _arr) => {
        let newItem: R = await fn(item, i, _arr);
        newArr.push(newItem);
    }).then(() => {
        return newArr;
    })
};
