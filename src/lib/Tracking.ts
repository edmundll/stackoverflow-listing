import _ from 'lodash';

export interface IEventBuilderParam {
    label?: string;
    value?: any;
}

export type EventBuilder = (param?: IEventBuilderParam) => {
    eventCategory: string;
    eventAction: string;
    eventLabel?: string;
    eventValue?: any;
}

export interface ITracking {
    eventBuilders: Dictionary<EventBuilder>;
}

export const Tracking: ITracking = {
    eventBuilders: {
        // btnClick() {
        //     return {
        //         eventCategory: '',
        //         eventAction: '',
        //         eventLabel: '',
        //     }
        // }
    },
};
