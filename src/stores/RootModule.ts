import { Module, Action, VuexModule, Mutation, getModule } from 'vuex-module-decorators';
import { IRootState } from '@/types/stores/RootState';

const StoreManagerPromise = import('@/plugins/StoreManager').then(m => m.StoreManager);

@Module({ stateFactory: true, namespaced: true, name: 'RootModule' })
export default class RootModule extends VuexModule implements IRootState {
    screenWidth: number = 0;
    screenHeight: number = 0;

    @Mutation setScreenSize(val: { width: number, height: number }): void {
        this.screenWidth = val.width;
        this.screenHeight = val.height;
    }
}
