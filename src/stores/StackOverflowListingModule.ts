import { IQuestion, IStackOverflowListingState, ITag, QuestionSorting } from '@/types/stores/StackOverflowListingState';
import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { ajv } from '@/plugins/Ajv';
import { axios } from '@/plugins/Axios';
import { TagArray } from '@/validationSchemas/Tag';
import { QuestionArray } from '@/validationSchemas/Question';

const StoreManagerPromise = import('@/plugins/StoreManager').then(m => m.StoreManager);

@Module({stateFactory: true, namespaced: true, name: 'StackOverflowListingModule'})
export default class StackOverflowListingModule extends VuexModule implements IStackOverflowListingState {

    fetching: boolean = false;

    tagKeyword: string = '';
    selectedTag: string = '';

    questionCurrentSorting: QuestionSorting = QuestionSorting.ACTIVITY;
    questionCurrentPage: number = 0;
    questionHasMore: boolean = true;
    trendingTags: ITag[] = [];
    questions: IQuestion[] = [];

    @Mutation setFetching(val: boolean): void {
        this.fetching = val;
    }

    @Mutation setTagKeyword(val: string): void {
        this.tagKeyword = val;
    }

    @Mutation setSelectedTag(val: string): void {
        this.selectedTag = val;
    }

    @Mutation setQuestionCurrentSorting(val: QuestionSorting): void {
        this.questionCurrentSorting = val;
    }

    @Mutation setQuestionCurrentPage(val: number): void {
        this.questionCurrentPage = val;
    }

    @Mutation setQuestionHasMore(val: boolean): void {
        this.questionHasMore = val;
    }

    @Mutation setTrendingTags(val: ITag[]): void {
        this.trendingTags = [...val];
    }

    @Mutation setQuestions(val: IQuestion[]): void {
        this.questions = [...val];
    }

    @Mutation appendQuestions(val: IQuestion[]): void {
        this.questions = [
            ...this.questions,
            ...val
        ];
    }

    @Action
    async fetchTrendingTags(): Promise<void> {
        const storeManager = await StoreManagerPromise;
        this.setFetching(true);
        this.setTagKeyword(this.tagKeyword.trim());
        this.setSelectedTag('');
        try {
            const result = await axios.get('https://api.stackexchange.com/2.2/tags', {
                params: {
                    site: 'stackoverflow',
                    page: 1,
                    pageSize: 10,
                    sort: 'popular',
                    order: 'desc',
                    inname: this.tagKeyword || undefined,
                }
            });
            if(!result || !result.items || !ajv.validate(TagArray, result.items)) {
                throw new Error('Unexpected Response');
            }
            this.setTrendingTags(result.items);
            if(!this.trendingTags.length) {
                return storeManager.notify.showAlertMessage({
                    title: 'TrendingTags',
                    message: 'Empty result set'
                }).then(() => {
                    this.setFetching(true);
                    this.setTagKeyword('');
                    return this.fetchTrendingTags();
                });
            }else{
                this.setSelectedTag(this.trendingTags.length ? this.trendingTags[0].name : '');
            }
        }catch(err){
            console.log(err);
            storeManager.notify.showAlertMessage({
                title: 'TrendingTags',
                message: `API Request Failed - ${err.message}`,
            });
        }finally{
            this.setFetching(false);
        }
    }

    @Action
    async fetchQuestions(): Promise<void> {
        if(!this.selectedTag || !this.questionHasMore) return;
        const storeManager = await StoreManagerPromise;
        this.setFetching(true);
        try {
            const result = await axios.get('https://api.stackexchange.com/2.2/questions', {
                params: {
                    site: 'stackoverflow',
                    page: this.questionCurrentPage + 1,
                    pageSize: 20,
                    sort: this.questionCurrentSorting,
                    order: 'desc',
                    tagged: this.selectedTag,
                }
            });
            if(!result || !result.items || !ajv.validate(QuestionArray, result.items)) {
                if(ajv.errors) {
                    console.log(ajv.errors);
                }
                throw new Error('Unexpected Response');
            }
            this.appendQuestions(result.items);
            this.setQuestionHasMore(!!result.has_more);
            this.setQuestionCurrentPage(this.questionCurrentPage + 1);
        }catch(err){
            console.log(err);
            storeManager.notify.showAlertMessage({
                title: 'Questions',
                message: `API Request Failed - ${err.message}`,
            });
        }finally{
            this.setFetching(false);
        }
    }
}
