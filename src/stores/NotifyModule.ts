import {Module, Action, VuexModule, Mutation, getModule} from 'vuex-module-decorators';
import { INotifyState, PopupMessage, PopupMessageConfig, PopupCancelError } from '@/types/stores/NotifyState';

const VueI18n = {
    t(str: string, ...params: any[]): string {
        return str.replace('popupMessage.', '');
    },
}
import Vue from 'vue';

const StoreManagerPromise = import('@/plugins/StoreManager').then(m => m.StoreManager);

@Module({ stateFactory: true, namespaced: true, name: 'NotifyModule' })
export default class NotifyModule extends VuexModule implements INotifyState {

    popupMessage: PopupMessage = {
        show: false,
        title: '',
        message: '',
        buttons: [],
        maxWidth: 0,
        maxHeight: 0,
        padding: 0,
        showClose: false,
        onClose: null,
    }

    @Mutation show(config: PopupMessageConfig) {
        this.popupMessage.title = config.title || '';
        this.popupMessage.message = config.message || '';
        Vue.set(this.popupMessage, 'buttons', config.buttons || []);
        this.popupMessage.maxWidth = config.maxWidth || 0;
        this.popupMessage.maxHeight = config.maxHeight || 0;
        this.popupMessage.padding = config.padding || 0;
        this.popupMessage.showClose = config.showClose || false;
        this.popupMessage.onClose = config.onClose || null;
        this.popupMessage.show = true;
    }

    @Mutation dismiss() {
        this.popupMessage.show = false;
        // this.popupMessage.onClose = null;
        // this.popupMessage.showClose = false;
        // this.popupMessage.title = '';
        // this.popupMessage.message = '';
        // Vue.set(this.popupMessage, 'buttons', []);
        // this.popupMessage.maxWidth = 0;
        // this.popupMessage.maxHeight = 0;
        // this.popupMessage.padding = 0;
    }

    @Action
    async showPopupMessage(config: PopupMessageConfig): Promise<void> {
        this.dismiss();
        this.show(config);
    }

    @Action
    async showConfirmMessage(configOverride?: PopupMessageConfig, confirmCb?: () => Promise<boolean | void>, cancelCb?: () => Promise<boolean | void>): Promise<void> {
        const p = new Promise<void>((resolve, reject) => {
            let _confirmCb = async () => {
                let dismiss: boolean | void = true;
                if (confirmCb) {
                    dismiss = await confirmCb();
                }
                if (dismiss) {
                    this.dismiss();
                }
                return resolve();
            };
            let _cancelCb = async () => {
                let dismiss: boolean | void = true;
                if (cancelCb) {
                    dismiss = await cancelCb();
                }
                if (dismiss) {
                    this.dismiss();
                }
                return reject(new PopupCancelError());
            };
            let config: PopupMessageConfig = {
                title: VueI18n.t('popupMessage.notice') as string,
                message: VueI18n.t('popupMessage.messages.confirm') as string,
                buttons: [
                    {
                        label: VueI18n.t('popupMessage.confirm') as string,
                        cb: _confirmCb,
                    },
                    {
                        label: VueI18n.t('popupMessage.cancel') as string,
                        cb: _cancelCb,
                    },
                ],
                maxWidth: 320,
                ...configOverride
            }
            this.showPopupMessage(config);
        });
        return p;
    }

    @Action
    async showAlertMessage(configOverride?: PopupMessageConfig, confirmCb?: () => Promise<boolean | void>, cancelCb?: () => Promise<boolean | void>): Promise<any> {
        const p = new Promise<void>((resolve, reject) => {
            let _confirmCb = async () => {
                let dismiss: boolean | void = true;
                if (confirmCb) {
                    dismiss = await confirmCb();
                }
                if (dismiss) {
                    this.dismiss();
                }
                return resolve();
            };
            let config: PopupMessageConfig = {
                title: VueI18n.t('popupMessage.notice') as string,
                message: VueI18n.t('popupMessage.messages.confirm') as string,
                buttons: [
                    {
                        label: VueI18n.t('popupMessage.confirm') as string,
                        cb: _confirmCb,
                    },
                ],
                maxWidth: 320,
                ...configOverride
            }
            this.showPopupMessage(config);
        });
        return p;
    }
}
