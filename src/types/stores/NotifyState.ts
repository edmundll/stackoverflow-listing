// export type Type = any[];
export interface PopupMessageButton {
    label: string;
    cb: (() => Promise<boolean | void>) | null;
}

export interface PopupMessage {
    show: boolean;
    title: string;
    message: string;
    buttons: PopupMessageButton[];
    maxWidth: number;
    maxHeight: number;
    padding: number;
    showClose: boolean;
    onClose: ((isForce: boolean) => Promise<boolean | void>) | null;
}

// export interface Alert {
//     show: boolean;
//     title: string;
//     message: string;
//     messageMarkdown: boolean;
//     confirmText: string;
//     cb: false | Function,
//     err: false | Function,
// }

// export interface Toast {
//     show: boolean;
//     message: string;
//     autoDismiss: number;
// }

export interface PopupMessageConfig extends Partial<Exclude<PopupMessage, PopupMessage['show']>> { }

// export interface AlertConfig extends Partial<Alert> { }

// export interface ToastConfig extends Partial<Toast> { }

export class PopupCancelError extends Error {

    constructor(...args: any[]) {
        super(...args);
    }
}

export interface INotifyState extends ExtendableObject {
    popupMessage: PopupMessage;
    // alert: Alert;
    // toast: Toast;
}
