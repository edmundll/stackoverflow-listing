export interface ITag {
    name: string;
    count?: number;
    has_synonyms?: boolean;
    is_moderator_only?: boolean;
    is_required?: boolean;
}

export interface IQuestionOwner {
    display_name: string;
    profile_image?: string;
    link?: string;
    reputation?: number;
    user_id?: number;
    user_type?: string;
}

export interface IQuestion {
    title: string;
    score: number;
    answer_count: number;
    view_count: number;
    owner: IQuestionOwner;
    link: string;
    tags?: string[];
    is_answered?: boolean;
    last_activity_date?: number;
    creation_date?: number;
    question_id?: number;
    content_license?: string;
}

export interface IStackOverflowListingState {
    fetching: boolean;

    tagKeyword: string;
    selectedTag: string;

    questionCurrentPage: number;
    questionHasMore: boolean;
    trendingTags: ITag[];
    questions: IQuestion[];
}

export enum QuestionSorting {
    ACTIVITY = 'activity',
    VOTES = 'votes',
    CREATION = 'creation',
    HOT = 'hot',
    WEEK = 'week',
    MONTH = 'month',
}
