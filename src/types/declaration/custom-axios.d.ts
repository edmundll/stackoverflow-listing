import Axios from 'axios';

declare module 'axios' {

    interface AxiosRequestConfig {
        raw?: boolean; // set to true to return the whole response object in response interceptor
    }

    interface AxiosResponse<T = any> extends Promise<T>, ExtendableObject { }
}
