import { ITracking } from '@/lib/Tracking';
import { IStoreManager } from '@/plugins/StoreManager';
import Ajv from 'ajv';
import { AxiosInstance } from 'axios';

declare global {

    interface Dictionary<T = any> {
        [key: string]: T;
    }

    interface NumDictionary<T = any> extends Dictionary<T> {
        [key: number]: T;
    }

    interface ExtendableObject extends Dictionary<any> { }

    interface Constructor<T> {
        new(...args: any[]): T;
    }

    type PromiseType<P> = P extends { then(onfulfilled?: (value: infer T) => unknown): unknown } ? T : P;

    type ParamType<F> = F extends (...args: infer T) => unknown ? T : any;

    interface Window {
        resizeTimeout?: number;
    }

//     interface GA extends ExtendableObject {
//         event(category: string, action: string, label?: string, value?: any): void;
//         event(params: { eventCategory: string, eventAction: string, eventLabel?: string, eventValue?: any}): void;
//     }
}

declare module 'vue/types/vue' {

    interface Vue {
        $ajv: Ajv.Ajv;
        $axios: AxiosInstance;
        $storeManager: IStoreManager;
        $style: ExtendableObject;
        $tracking: ITracking;
        $t: Function;
        $tc: Function;
    }
}

export { };

