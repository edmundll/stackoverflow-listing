import {FieldOption, ParsedFieldOption} from '@/types/components/DynamicField'

export interface FormTemplate {
    name: string;
    label: string;
    fields: FieldOption[];
}

export interface ParsedFormTemplate extends FormTemplate {
    fields: ParsedFieldOption[];
}
