export enum InputType {
    TEXT = 'text',
    PASSWORD = 'password',
    TEXTAREA = 'textarea'
}

export enum ErrorDisplayMode {
    ALL = 'all',
    SINGLE_MENU = 'singleMenu',
    HIDE = 'hide',
}
