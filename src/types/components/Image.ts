export enum ContainerType {
    IMAGE = 'img',
    DIV = 'div',
}

export enum ContainerMode {
    COVER = 'cover',
    CONTAIN = 'contain',
    FULL = 'full-size',
}

export enum InitiateStatus {
    INITIATING,
    INITIATED,
    NOT_INITIATED,
    FAILED,
}
