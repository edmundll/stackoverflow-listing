import ValidationRule, { ValidationRuleMessage } from '@/types/ValidationRule';
import { SelectOption } from '@/types/components/SelectInput';

export type ArrayInitDataType = ExtendableObject | string | any[];

export enum DynamicFieldType {
    // EMPTY = 'empty',
    // BOOLEAN = 'boolean',
    SEPARATOR = 'separator',
    INTEGER = 'integer',
    FLOAT = 'float',
    STRING = 'string',
    PASSWORD = 'password',
    DATE = 'date',
    EMAIL = 'email',
    PHONE = 'phone',
    LIST = 'list',
    ARRAY = 'array',
    CUSTOM = 'custom',
}

export interface FieldOptionBase {
    templateRootName?: string;
    type: DynamicFieldType;
    name: string;
    _name?: string;
    parent?: string;
    label?: string;
    description?: string;
    errorMessage?: ValidationRuleMessage;
    validator?: (v: any) => boolean;
    rules?: ValidationRule[];
    hidden?: boolean;
    disabled?: boolean;
    required?: boolean;
    requiredIf?: string;
    requiredIfFn?: () => boolean;
    row?: number;
    column?: number;
}

export interface IntegerFieldOption extends FieldOptionBase {
    type: DynamicFieldType.INTEGER;
    min?: number;
    max?: number;
    size?: number;
    regex?: RegExp;
}

export interface FloatFieldOption extends FieldOptionBase {
    type: DynamicFieldType.FLOAT;
    min?: number;
    max?: number;
    size?: number;
    regex?: RegExp;
}

export interface StringFieldOption extends FieldOptionBase {
    type: DynamicFieldType.STRING;
    min?: number;
    max?: number;
    size?: number;
    regex?: RegExp;
    multiline?: boolean;
}

export interface PasswordFieldOption extends FieldOptionBase {
    type: DynamicFieldType.PASSWORD;
    min?: number;
    max?: number;
    size?: number;
}

export interface ListFieldOption extends FieldOptionBase {
    type: DynamicFieldType.LIST;
    options: SelectOption[];
    multiple?: boolean;
    min?: number;
    max?: number;
    size?: number;
    singleLine?: boolean;
}

export interface ArrayFieldOption extends FieldOptionBase {
    type: DynamicFieldType.ARRAY;
    typeOf: FieldOption;
    initFn?: () => ArrayInitDataType | Array<ArrayInitDataType>;
    min?: number;
    max?: number;
    size?: number;
}

export interface CustomFieldOption extends FieldOptionBase {
    type: DynamicFieldType.CUSTOM;
    children: FieldOption[];
}

export interface OtherFieldOption extends FieldOptionBase {
    type: Exclude<DynamicFieldType, DynamicFieldType.INTEGER | DynamicFieldType.FLOAT | DynamicFieldType.STRING | DynamicFieldType.PASSWORD | DynamicFieldType.LIST | DynamicFieldType.ARRAY | DynamicFieldType.CUSTOM>;
}

export type FieldOption = IntegerFieldOption | FloatFieldOption | StringFieldOption | PasswordFieldOption | ListFieldOption | ArrayFieldOption | CustomFieldOption | OtherFieldOption;

export interface ParsedArrayFieldOption extends ArrayFieldOption {
    typeOf: ParsedFieldOption;
}

export interface ParsedCustomFieldOption extends CustomFieldOption {
    children: ParsedFieldOption[];
}

export type ParsedFieldOption = (Exclude<FieldOption, ArrayFieldOption | CustomFieldOption> | ParsedArrayFieldOption | ParsedCustomFieldOption) & Required<Pick<FieldOption, 'templateRootName' | '_name' | 'rules'>>;
