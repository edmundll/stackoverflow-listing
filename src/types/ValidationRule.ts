// export type Type = any[];

export type ValidationRuleType =
'REQUIRED' |
'REQUIRED_IF' |
'MIN' |
'MAX' |
'MINMAX' |
'SIZE' |
'PHONE' |
'EMAIL' |
'DATE' |
'TIME' |
'INTEGER' |
'FLOAT' |
'REGEX' |
'IN' |
'ARRAY_REQUIRED' |
'ARRAY_MIN' |
'ARRAY_MAX' |
'ARRAY_MINMAX' |
'ARRAY_SIZE' |
'CUSTOM_VALIDATOR';

export type ValidationRuleMessage = string | {
    [key in (ValidationRuleType | 'default')]?: string;
}

export interface ValidationRuleParam<T> extends ExtendableObject {
    label?: string;
    message?: ValidationRuleMessage;
    min?: number;
    max?: number;
    size?: number;
    regex?: RegExp;
    values?: any[];
    ifFunction?: { (...args: any[]): boolean };
    validator?: { (v: T): boolean };
}

export default interface ValidationRule {
    <T>(v: T, params?: ValidationRuleParam<T>): true | string;
}
