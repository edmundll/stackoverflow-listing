export const Tag = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            minLength: 1,
        },
    },
    required: ['name']
}

export const TagArray = {
    type: 'array',
    items: Tag,
}
