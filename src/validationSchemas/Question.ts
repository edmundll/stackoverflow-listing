export const Question = {
    type: 'object',
    properties: {
        title: {
            type: 'string',
            minLength: 1,
        },
        score: {
            type: 'number'
        },
        answer_count: {
            type: 'number',
        },
        view_count: {
            type: 'number',
        },
        owner: {
            type: 'object',
            properties: {
                display_name: {
                    type: 'string',
                    minLength: 1,
                },
            },
            required: ['display_name']
        },
        link: {
            type: 'string',
            minLength: 1,
        }
    },
    required: ['title', 'score', 'answer_count', 'view_count', 'owner', 'link']
};

export const QuestionArray = {
    type: 'array',
    items: Question,
}
