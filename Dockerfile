FROM node:13.8.0-alpine

RUN mkdir -p /stackoverflow-listing
WORKDIR /stackoverflow-listing

RUN apk add --no-cache git
RUN apk add --no-cache tzdata
RUN export TZ=Asia/Hong_Kong

COPY ./package.json ./package.json
COPY ./yarn.lock ./yarn.lock
RUN yarn install

COPY . ./
RUN yarn build

CMD ["yarn", "local-deploy"]
EXPOSE 8080/tcp
